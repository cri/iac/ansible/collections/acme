#! /usr/bin/env bash

set -euo pipefail

export VAULT_ADDR='{{ vault_addr }}'

export VAULT_TOKEN="$(
  vault write \
    -field=token \
    auth/{{ acme_vault_approle_path }}/login \
    role_id='{{ acme_vault_upload_role_id }}' \
    secret_id='{{ acme_vault_upload_secret_id }}'
)"

vault kv put "{{ acme_vault_certs_vault_path }}" \
  chain.pem=@chain.pem \
  fullchain.pem=@fullchain.pem \
  full.pem=@full.pem \
  key.pem=@key.pem
