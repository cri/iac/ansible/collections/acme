#! /usr/bin/env bash

set -euo pipefail

echo '{{ domain_hash }}' > domainhash.txt

# Check if we can renew
if [ -e 'certificates/{{ item.domain }}.key' -a -e 'certificates/{{ item.domain }}.crt' ]; then
  # When domains are updated, there's no need to do a full Lego run, but it's
  # likely renew won't work if days is too low.
  if [ -e certificates/domainhash.txt ] && cmp -s domainhash.txt certificates/domainhash.txt; then
    lego {{ lego_cmd_opts }} renew --reuse-key --days {{ acme_valid_min_days }}
  else
    # Any number > 90 works, but this one is over 9000 ;-)
    lego {{ lego_cmd_opts }} renew --reuse-key --days 9001
  fi
else
  lego {{ lego_cmd_opts }} run
fi

mv domainhash.txt certificates/
chmod 640 certificates/*
chmod -R 700 accounts/*

# In case we ever change the group
chown 'acme:acme' certificates/*

# Copy all certs to the "real" certs directory
CERT='certificates/{{ item.domain }}.crt'
if [ -e "${CERT}" ] && ! cmp -s "${CERT}" out/fullchain.pem; then
  touch out/renewed
  echo Installing new certificate
  cp -vp 'certificates/{{ item.domain }}.crt' out/fullchain.pem
  cp -vp 'certificates/{{ item.domain }}.key' out/key.pem
  cp -vp 'certificates/{{ item.domain }}.issuer.crt' out/chain.pem
  ln -sf fullchain.pem out/cert.pem
  cat out/key.pem out/fullchain.pem > out/full.pem
fi
