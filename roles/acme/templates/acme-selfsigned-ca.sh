#! /usr/bin/env bash

{{ acme_state_dir }}/bin/minica \
  --ca-key ca/key.pem \
  --ca-cert ca/cert.pem \
  --domains selfsigned.local

chmod 600 ca/*
