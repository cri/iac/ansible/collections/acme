#! /usr/bin/env bash

/srv/acme/bin/minica \
  --ca-key ca/key.pem \
  --ca-cert ca/cert.pem \
  --domains '{{ item.domain }}'{% for name in extra_names %} --domains '{{ name }}'{% endfor %}

cd '{{ item.domain }}'
cp ../ca/cert.pem chain.pem
cat cert.pem chain.pem > fullchain.pem
cat key.pem fullchain.pem > full.pem

chmod 640 *

# In case the group changes
chown 'acme:acme' *
