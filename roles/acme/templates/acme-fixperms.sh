#! /usr/bin/env bash

{% for item in acme_certificates %}
for fixpath in {{ acme_state_dir }}/'{{ item.domain }}' {{ acme_state_dir }}/.lego/'{{ item.domain }}'; do
  if [ -d "$fixpath" ]; then
    chmod -R 750 "$fixpath"
    chown -R acme:acme "$fixpath"
  fi
done
{% endfor %}
