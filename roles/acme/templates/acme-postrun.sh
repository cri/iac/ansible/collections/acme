#! /usr/bin/env bash

cd {{ acme_state_dir }}/'{{ item.domain }}'

export ACME_DOMAIN='{{ item.domain }}'

if [ -e renewed ]; then
  rm renewed
  {{ item.postrun | d(acme_postrun) }}
fi
