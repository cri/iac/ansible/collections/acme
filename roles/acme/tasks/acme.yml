---

# Those services will be recreated by the role later on.
# For non domain specific services, they are configured to not overwrite any
# existing files
# For domain specific services, they are configured to not renew the
# certificates if they are still valid for acme_valid_min_days or more. Also,
# the configuration is idempotent thanks to the hashes computed at the
# beginning of domain.yml
- name: List all ACME services
  find:
    path: /etc/systemd/system
    patterns: "acme-*"
  register: acme_services
- name: Add existing services to dictionary
  set_fact:
    acme_services_to_keep: "{{ acme_services_to_keep | default({})
                            | combine({ item: false }) }}"
  with_items: "{{ acme_services.files | map(attribute='path') | list }}"

- name: Copy ACME scripts
  template:
    src: "{{ item }}"
    dest: "{{ acme_state_dir }}/scripts/{{ item }}"
    owner: acme
    group: acme
    mode: 0755
  loop:
    - acme-fixperms.sh
  register: copy_acme_scripts

- name: Copy ACME self-signed scripts
  template:
    src: "{{ item }}"
    dest: "{{ acme_state_dir }}/scripts/{{ item }}"
    owner: acme
    group: acme
    mode: 0755
  when: acme_preliminary_selfsigned
  loop:
    - acme-selfsigned-ca.sh
  register: copy_acme_selfsigned_scripts

- name: Create ACME self-signed CA directory
  file:
    state: directory
    path: "{{ acme_state_dir }}/.minica"
    owner: acme
    group: acme
    mode: 0755

- name: Create ACME Lego directory
  file:
    state: directory
    path: "{{ acme_state_dir }}/.lego"
    owner: acme
    group: acme
    mode: 0755

- name: Copy ACME services
  template:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item }}"
    mode: 0644
  loop:
    - acme-fixperms.service
  register: copy_acme_services

- name: Save internal state for ACME services
  changed_when: false
  vars:
    service_file: "/etc/systemd/system/{{ item }}"
  set_fact:
    acme_services_to_keep: "{{ acme_services_to_keep | default({})
                            | combine({ service_file: true }) }}"
  loop:
    - acme-fixperms.service
  register: copy_acme_selfsigned_services

- name: Copy ACME self-signed services
  template:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item }}"
    mode: 0644
  loop:
    - acme-selfsigned-ca.service
  when: acme_preliminary_selfsigned

- name: Save internal state for ACME self-signed services
  changed_when: false
  vars:
    service_file: "/etc/systemd/system/{{ item }}"
  set_fact:
    acme_services_to_keep: "{{ acme_services_to_keep | default({})
                            | combine({ service_file: true }) }}"
  loop:
    - acme-selfsigned-ca.service
  when: acme_preliminary_selfsigned

- name: Reload systemd
  systemd:
    daemon_reload: true

- name: Enable ACME services
  systemd:  # noqa no-handler
    name: "{{ item }}"
    enabled: true
  loop:
    - acme-fixperms

- name: Start ACME services
  systemd:  # noqa no-handler
    name: "{{ item }}"
    state: started
  loop:
    - acme-fixperms
  when: acme_always_start_cert_services
         or copy_acme_scripts.changed
         or copy_acme_services.changed

- name: Enable ACME self-signed services
  systemd:
    name: "{{ item }}"
    enabled: true
  loop:
    - acme-selfsigned-ca
  when:
    - acme_preliminary_selfsigned

- name: Start ACME self-signed services
  systemd:
    name: "{{ item }}"
    state: started
  loop:
    - acme-selfsigned-ca
  when:
    - acme_preliminary_selfsigned
    - acme_always_start_cert_services
        or copy_acme_selfsigned_scripts.changed
        or copy_acme_selfsigned_services.changed
