#! /usr/bin/env bash

set -euo pipefail

export VAULT_ADDR='{{ vault_addr }}'

export VAULT_TOKEN="$(
  set -e
  vault write \
    -field=token \
    auth/{{ acme_vault_approle_path }}/login \
    role_id='{{ acme_vault_client_role_id }}' \
    secret_id='{{ acme_vault_client_secret_id }}'
)"

one_cert_has_changed="false"
current_cert_has_changed="false"

cd '{{ acme_vault_client_cert_dir }}'

{% for cert in acme_vault_client_certs %}
export ACME_DOMAIN="{{ cert.domain }}"
mkdir -p "${ACME_DOMAIN}"
pushd "${ACME_DOMAIN}"

current_cert_has_changed="false"

for file in chain.pem full.pem fullchain.pem key.pem; do
  vault kv get -field="${file}" "{{ cert.vault_path | d(acme_vault_certs_vault_path) }}" > "${file}.tmp"
  if [ ! -e "${file}" ] || ! cmp -s "${file}.tmp" "${file}"; then
    mv "${file}.tmp" "${file}"
    current_cert_has_changed="true"
    one_cert_has_changed="true"
  fi
  rm -f "${file}.tmp"
done
chown -R '{{ cert.user | d(acme_vault_client_user) }}:{{ cert.group | d(acme_vault_client_group)}}' .
chmod -R '{{ cert.mode | d(acme_vault_client_mode) }}' .

if $current_cert_has_changed; then
  echo Certificate for "${ACME_DOMAIN}" has changed. Running postrun if defined.
  {{ cert.postrun | d('') }}
fi
popd
{% endfor %}

if $one_cert_has_changed; then
  echo At least one certificate has changed. Running global postrun if defined.
  {{ acme_vault_client_postrun }}
fi
