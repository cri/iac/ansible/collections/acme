---

- name: Create certificates directory
  file:
    state: directory
    path: "{{ acme_vault_client_cert_dir }}"
    owner: "{{ acme_vault_client_user }}"
    group: "{{ acme_vault_client_group }}"
    mode: "{{ acme_vault_client_mode }}"

- name: Copy vault download script
  template:
    src: acme-vault-download.sh
    dest: /usr/local/bin/acme-vault-download.sh
    owner: root
    group: root
    mode: 0755
  register: copy_vault_dl_script

- name: Copy vault download service
  template:
    src: acme-vault-download.service
    dest: /etc/systemd/system/acme-vault-download.service
    owner: root
    group: root
    mode: 0644
  register: copy_vault_dl_service

- name: Copy vault download timer
  template:
    src: acme-vault-download.timer
    dest: /etc/systemd/system/acme-vault-download.timer
    owner: root
    group: root
    mode: 0644
  register: copy_vault_dl_timer

- name: Remove cronjob for renewing certificates
  cron:
    state: absent
    name: Renew certificates from vault
    special_time: "{{ acme_vault_client_renew_interval }}"
    job: "/usr/local/bin/acme-vault-download.sh"

- name: "Enable vault download service and timer"
  systemd:  # noqa no-handler
    name: "{{ service }}"
    enabled: true
  loop:
    - "acme-vault-download.service"
    - "acme-vault-download.timer"
  loop_control:
    loop_var: service

- name: "Start vault download timer"
  systemd:  # noqa no-handler
    name: acme-vault-download.timer
    state: started

- name: "Start vault download service if script changed"
  systemd:  # noqa no-handler
    name: acme-vault-download.service
    state: started
  when: acme_vault_client_always_fetch_certs
         or copy_vault_dl_script.changed
         or copy_vault_dl_service.changed
         or copy_vault_dl_timer.changed

- name: Reload systemd
  systemd:
    daemon_reload: true
